// All dimensions in mm

include <BOSL2/std.scad>

//**************************************************************************************
//*        GLOBAL PARAMETERS                                                           *
//**************************************************************************************
// Print debugging info to Console
DEBUG = true;

// QUALITY factor (segments in curves like circles)
QUALITY = 500;

// See Note at end of https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/CSG_Modelling#union
// Epsilon is the overlap global in mm that we will apply to joining or difference edges
EPSILON = 0.1;

//**************************************************************************************
//*        USER PARAMETERS                                                           *
//**************************************************************************************
// Dovetails_Bottom - Do we build dovetails along the bottom
Dovetails_Bottom = true;
// Dovetails_Top - Do we build dovetails along the top
Dovetails_Top = true;
// Dovetails_Left - Do we build dovetails along the left
Dovetails_Left = true;
// Dovetails_Right - Do we build dovetails along the right
Dovetails_Right = true;

// Number of jars widthways
Number_X = 2;
// Number of jars depthways
Number_Y = 2;
// Thickness of grid
Thickness = 4;
// 2nd row right offset - true, or set false for 2nd row having a left offset
Second_Row_Right_Offset = true;
// Legs - List of [x,y] positions for legs.
Legs = [
  [1,1],
  ] ;
// Leg_Diameter in mm
Leg_Diameter = 5;
// Leg_Length
Leg_Length = 20;

// Distance between edges of jars
Jar_Spacing = 4;
// Diameter of jar hole
Jar_Diameter = 50;
// Dovetail Pin Width
Dovetail_Width = 4;
// Dovetail Pin Width
Dovetail_Height = 2;
// Dovetail Angle
Dovetail_Angle = 70;
// Dovetail inset from edge of hexagon to edge of dovetail long edge
Dovetail_Inset = 2;
// Dovetail Fit Clearance in mm: 0 = none
Dovetail_Clearance = 0.05;

//**************************************************************************************
//*       VARIABLES                                                                    *
//**************************************************************************************

//**************************************************************************************
//        DEBUG Variables - Output to Console                                          *
//**************************************************************************************

//**************************************************************************************
//*       FUNCTIONS                                                                    *
//**************************************************************************************

// Dovetail Vector isTab = 1 for tab, -1 for cutout
function dovetailVector(isTab, width, height, angle=70, clearance) =
  let()
  [
    [-width/2 + height/tan(angle) + isTab*clearance/2, 0],
    [-width/2 + (isTab * clearance/2) , isTab * (height - isTab*clearance/2)],
    [width/2 - (isTab * clearance/2) , isTab * (height - isTab*clearance/2)],
    [width/2 - height/tan(angle) - isTab*clearance/2, 0],
  ];

function _hexEdgePoints(jarDiameter, jarSpacing, dovetailWidth, dovetailHeight, dovetailAngle, dovetailInset, dovetailClearance, features, side) =
  let (hexWidth = jarDiameter + jarSpacing)
  let (flatToCentre = hexWidth/2)
  let (sideLength = 2*flatToCentre/tan(60))
  let (dovetailTab=dovetailVector(isTab=1, width=dovetailWidth, height=dovetailWidth/2, angle=dovetailAngle, clearance = dovetailClearance))
  let (dovetailCut=dovetailVector(isTab=-1, width=dovetailWidth, height=dovetailWidth/2, angle=dovetailAngle, clearance = dovetailClearance))

  let (points = 
    assert(features=="d" || features=="p" || features=="e", str("Illegal side feature ", features, " for side ", side))
    features=="d" 
    ? // Generate side with dovetails
      concat(
        [[0,0]],                                                          // Start
        move([dovetailInset+dovetailWidth/2,0], dovetailTab),             // Move to dovetail tab and compute
        move([sideLength-dovetailInset-dovetailWidth/2,0], dovetailCut)  // Move to dovetail cut and compute
      )
    :   
      features=="e" 
      ? // Generate plain sides but with slight epsilon offsets so that they join cleanly
        concat(
        [[0,0]],                                                          // Start
        [[0, EPSILON]],
        [[sideLength, EPSILON]]
      )
      :
        features=="p" 
        ? // Generate plain sides only
          concat(
          [[0,0]]                                                          // Start
        )
        :
          [] // assert above should stop us getting here
  )
  zrot( -(side * 60 + 30), 
      move([0,flatToCentre],
        move([-sideLength/2, 0],
          points
        )
      )
    )
;

function _hexPoints (jarDiameter, jarSpacing, dovetailWidth, dovetailHeight, dovetailAngle, dovetailInset, dovetailClearance, sideFeatures, i=0, points=[]) =
  [ 
    each for( i = [0 : 5] )
      _hexEdgePoints(jarDiameter, jarSpacing, dovetailWidth, dovetailHeight, dovetailAngle, dovetailInset, dovetailClearance, sideFeatures[i], i)
  ]
;
//**************************************************************************************
//*       MODULES                                                                    *
//**************************************************************************************

// Single Unit Jar Holder
module singleUnit(thickness, jarDiameter, jarSpacing, dovetailWidth, dovetailHeight , dovetailAngle, dovetailInset, dovetailClearance, sideFeatures ) {
  hexPoints = _hexPoints( jarDiameter, jarSpacing, dovetailWidth, dovetailHeight , dovetailAngle, dovetailInset, dovetailClearance, sideFeatures );

  difference() {
    linear_extrude(height = thickness) { 
        polygon(hexPoints);
    }
    // Cut the circular opening
    zmove(-EPSILON)
    linear_extrude(height = thickness+2*EPSILON) { 
        circle(d=jarDiameter, $fn=QUALITY);
      }
  }
}

module singleLeg(diameter, height) {
  translate([0,0,EPSILON]) mirror([0,0,1]) cylinder(h = height + EPSILON, r = diameter/2, $fn=QUALITY);
}

//**************************************************************************************
//        CONSTRUCT GEOMETRY
//**************************************************************************************

//singleUnit(Thickness, Jar_Diameter, Jar_Spacing, Dovetail_Width, Dovetail_Height, Dovetail_Angle, Dovetail_Inset, Dovetail_Clearance, ["d","d","d","d","d","d"]);
unitSpacing = Jar_Diameter + Jar_Spacing;

// Add legs
for (i = [0 : len(Legs)-1]) {
  l = Legs[i];
  x = l[0];
  y = l[1];
  xLegOffset = (x)*unitSpacing + (y % 2 == 0 ? 0 : unitSpacing/2) * (Second_Row_Right_Offset ? -1 : 1) + unitSpacing/2;
  yLegOffset = y* (unitSpacing/2*tan(60));
  color("red") translate([xLegOffset, yLegOffset, 0]) singleLeg(diameter = Leg_Diameter, height = Leg_Length);
}

for (y = [0 : Number_Y-1]) {
  for (x = [0 : Number_X-1]) {
    featureList = [
      y==Number_Y-1 ? (Dovetails_Top ? "d" : "p") : "p", // Top Right
      x==Number_X-1 ? (Dovetails_Right ? "d" : "p") : "p", // Right
      y==0 ? "p" : "e", // Bottom Right
      y==0 ? (Dovetails_Bottom ? "d" : "p") : "e", // Bottom Left
      x==0 ? (Dovetails_Left ? "d" : "p") : "e", // Left
      "p", // Top Left
    ];

    xOffset = x*unitSpacing + (y % 2 == 0 ? 0 : unitSpacing/2) * (Second_Row_Right_Offset ? 1 : -1) + unitSpacing/2 ;
    yOffset = y* (unitSpacing/2*tan(60)) + unitSpacing/sqrt(3);
    color("green", 0.5) translate([xOffset, yOffset, 0]) singleUnit(Thickness, Jar_Diameter, Jar_Spacing, Dovetail_Width, Dovetail_Height, Dovetail_Angle, Dovetail_Inset, Dovetail_Clearance, featureList);
  }
}

