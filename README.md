# README - HexiSpice Jar Grid for Drawer

Simple grid to keep jars arranged nicely in a drawer.

## Intro

I use Sainsburys' (UK supermarket) spice jars - they're medium, round, reusable. 

But put into a drawer, they shift around every time the drawer is opened of closed
leading to a disorgansied mess.

I wanted something that was parametric, could be clipped together to form arbitratry 
sized organsier grids to suit any drawer, regardless of printer capacity.

To that end, this model is developed in OpenSCAD (after roughing out some ideas in FreeCAD)
as it is easier to do serious parametric stuff in OpenSCAD.

I will keep some ready to go STL models here for convenience so it would be possible to take just
say a 2x2 grid and clip together until you have what you need.

## Feature overview

*   [x] **Modular** can build any grid layout up to the limit of your printer
*   [x] **Extendeable** can clip together sections to make larger sections to any size
*   [x] **Features** fully parameter driven
*   [x] **OpenSCAD** pure OpenSCAD code
*   [x] **Quickstart** Ready to go STL files are on https://www.printables.com/model/573780-spice-drawer-organiser-with-source-code

## Contents

*   [Pictures](#pictures)
*   [Just Print Me Something, Dammit!](#just-print-me-something-dammit)
*   [Printing](#printing)
*   [Customising](#customising)
*   [Bugs](#bugs)
*   [License](#license)

## Pictures

![Completed](https://gitlab.com/timjwatts-public/household-widgets/hexispice/-/raw/master/Photos/Complete.jpg?ref_type=heads)

## Just Print Me Something, Dammit!

Select one or more of the Generic STL downloads and print. These assume a 50mm max jar diameter, 20mm raised legs and have dovetail press fit clips on all sides. Using a combination of 2 and 3 dimensions, any size from 2x2 upwards will be possible

## Printing

* Prints fine in PLA. I have tested this on the 0.2mm QUALITY setting on a Prusa Mk4 with a 0.4mm nozzle and the dovetails press fit by hand.

* If the dovetails are too tight, try increasing the Clearance parameter in OpenSCAD or tweak the print with a file or sandpaper.

* The dovetails should not be loose, but if they are, a drop of glue can be used.

## Customising

* Install the BOLS2 library for OpenSCAD from here https://github.com/BelfrySCAD/BOSL2/
* Make sure you have OpenSCAD installed (v2021.1 is what I used, later should be fine too )
* Open the HexiSpice.scad file in OpenSCAD
* Tweak the parameters as you wish. The ones you are most likely to want to change are:
  * Number_X - the number of jars from left to right
  * Number_Y - the number of jar rows
  * Jar_Diameter in mm
  * Leg_length in mm
  * Legs - this is tricky to describe.
    * It cannot (yet) be edited reliably in the OpenSCAD Customizer - you will need to tweak the source code
    * It is a list of points to place the legs in a simple integer offset
    * Legs will be placed at jar corners where material is thickest
    * [[1,1]] will give you a single leg
    * [1,1], [2,2]] will give you two legs
    * Use the openSCAD viewer to see if you get what you need
  * Render
  * Export, slice and print!

## Bugs
* Spurious dovetails appear which do not get used by actual joins - dovetail logic needs refining - not serious and repeatable, not random
* Legs cannot be placed in every position that makes sense - needs improvement.

## License
[Creative Commons ShareAlike v4.0](https://choosealicense.com/licenses/cc-by-sa-4.0/)
